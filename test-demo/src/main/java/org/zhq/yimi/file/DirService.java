package org.zhq.yimi.file;

import java.io.File;

/**
 * 说明：
 *
 * @author zhq
 * @create 2017/5/30 0030
 */
public class DirService {

    public static void main(String[] args) {
        String filePath="G:\\test\\WebRoot\\WEB-INF\\view\\mobile";
        makeDir(filePath);
    }

    public static void makeDir(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            file.mkdirs();
            System.out.println("创建文件夹目录成功！" + filePath);
        } else {
            System.out.println("文件夹已经存在" + filePath);
        }
    }

}
