package org.zhq.yimi.file;

import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;
import org.jb2011.lnf.beautyeye.ch3_button.BEButtonUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * 说明：
 *
 * @author zhq
 * @create 2017/5/30 0030
 */
public class DirForm {

    private JTextField textField_rootPath;
    private JTextArea textArea_subPaths;
    private JButton button_doGen;
   /* private JLabel label_result;*/

    public static void main(String[] args) {

        try {
            org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.launchBeautyEyeLNF();
            //设置本属性将改变窗口边框样式定义
            // BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.osLookAndFeelDecorated;
            //强立体半透明
            //   BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.translucencyAppleLike;
            //弱立体感半透明	translucencySmallShadow
            BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.translucencySmallShadow;
            //普通不透明	generalNoTranslucencyShadow
            //   BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.generalNoTranslucencyShadow;

            //说明： 该按钮目前仅作为演示窗口标题按钮的自定义能力之用，未来将开放自定义功能，目前你可选择隐藏之。
            UIManager.put("RootPane.setupButtonVisible", false);
        } catch (Exception e) {
            e.printStackTrace();
            //TODO exception
        }

        JFrame frame = new JFrame("DirForm");
        frame.setContentPane(new DirForm().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();

        //add by zhq
        frame.setTitle("快速创建文件夹---(小强出品)");
        frame.setSize(1000, 600);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);

        // set frame full transparentframe.setUndecorated(true);AWTUtilities.setWindowOpaque(frame, false);
        //frame.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);

        frame.setVisible(true);
    }

    private JPanel mainPanel;
    private JButton button_openDir;
    private JTextField textField_result;

    /**
     * 初始化一些个性化界面参数
     */
    public void init(){
        //设置不用颜色的按钮
        button_doGen.setUI(new BEButtonUI().setNormalColor(BEButtonUI.NormalColor.lightBlue));
        button_openDir.setUI(new BEButtonUI().setNormalColor(BEButtonUI.NormalColor.lightBlue));



    }

    public DirForm() {
        init();

        button_doGen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String rootPath = textField_rootPath.getText();
                if (rootPath == null || rootPath.equals("")) {
                    textField_result.setText("请输入根目录");
                    return;
                }
                if (!(rootPath.endsWith("//") || rootPath.endsWith("\\"))) {
                    rootPath += "//";
                }

                String subPaths = textArea_subPaths.getText();
                if (subPaths == null || subPaths.equals("")) {
                    textField_result.setText("请输入子目录");
                    return;
                }
                String subPathss[] = subPaths.split("\n");
                for (String subPath : subPathss) {
                    String realPath = rootPath + "\\" + subPath;
                    DirService.makeDir(realPath);
                }
                textField_result.setText("执行成功");
            }
        });
        button_openDir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String rootPath = textField_rootPath.getText();
                File file = new File(rootPath);
                try {
                    Desktop.getDesktop().open(file.getParentFile());
                } catch (Exception e1) {
                    textField_result.setText("打开根文件夹失败！" + e1.getMessage());
                    //e1.printStackTrace();
                }
            }
        });
    }
}
